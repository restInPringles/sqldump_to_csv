# Sqldump to csv

Converts dupms of sql databases to csv (what else did you expect?). Heavily enspired by [mysqldump-to-csv](https://github.com/jamesmishra/mysqldump-to-csv) with intention to make it faster and more feature-rich.

## Usage
```bash
./sqldump_to_csv dump.sql > dump.csv
```

## Building

For this you'll need [the Rust toolchain](https://www.rust-lang.org/learn/get-started).

```bash
cargo build --release
```

After that you should get `target/release/sqldump_to_csv`.

## Contributing

You're welcome to improve the project in any way, i.e. by making pull requests, reporting issues, asking for more features, and so on.

## Why Rust

1. Perfomance: in my case this program processed a 40gb dump in about half an hour while implementation in python takes twise as much to process a 20gb dump;
2. UTF-8 by default: I don't need to put any effort into processing weird stuff that can be found in some dupms.

## Todo

- [ ] refactor the algorithm
- [ ] proper return codes, main shouldn't return value
- [ ] process having multiple tables in the same file
- [ ] optionally exclude unneeded entries
- [ ] write to file
- [ ] read from stdio
- [ ] generate header from schema
- [x] process LOCK TABLES WRITE
- [x] process multiline inserts
- [x] basic logic
