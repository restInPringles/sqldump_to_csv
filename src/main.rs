use std::env;
use std::error::Error;
use std::fs;
use std::io::BufRead;
use std::io::BufReader;
use std::str::Chars;
// use std::thread;
// use std::io::Write;
// use std::process;

fn process_quote(chars: &mut Chars) -> String {
    let mut res = String::new();
    let mut count_of_commas = 0;

    while let Some(chr) = chars.next() {
        if chr == '\\' {
            res.push(chr);
            res.push(chars.next().unwrap());
        }
        if chr == '\'' {
            break;
        }
        count_of_commas += (chr == ',') as u64;
        res.push(chr);
    }

    if count_of_commas > 0 {
        res.push('"');
        res.insert(0, '"');
    } 
        res
}

fn process_values(values: &str) {
    let mut iter = values.chars();
    while let Some(chr) = iter.next() {
        if chr == '(' {
            // new row
            let mut row = String::new();
            while let Some(chr) = iter.next() {
                if chr == '\'' {
                    row.push_str(&process_quote(&mut iter));
                    continue;
                }
                if chr == ')' {
                    // end of the row
                    println!("{}", row);
                    iter.next();
                    break;
                }
                if chr == 'N' {
                    // skip NULL
                    let mut val = String::with_capacity(4);
                    val.push(chr);
                    for _ in 0..3 {
                        val.push(iter.next().unwrap());
                    }
                    if val == "NULL" {
                        continue;
                    } else {
                        row += &val;
                    }
                }
                row.push(chr);
            }
        } else {
            panic!("SOMETHING WENT WRONG: '{}'", chr);
        }
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();
    //if args.len() != 3 {
    //    println!("Usage: sqldump_to_csv input_file output_file");
    //    process::exit(1);
    //}

    let inp = fs::File::open(&args[1])?;
    //let mut out = fs::File::create(&args[2])?;

    let mut multiline_insert = false;
    let mut lock_write = false;

    for line in BufReader::new(inp).lines() {
        if let Ok(line) = line {
            let line = line.trim();
            if multiline_insert {
                // the last character is either ';' or ','
                // 1st implies the end of the statement
                // 2nd should be just omitted
                let mut chars = line.chars();
                if chars.next_back() == Some(';') {
                    multiline_insert = false;
                }
                process_values(&chars.as_str());
            } else if line.starts_with("INSERT INTO") {
                let splited: Vec<&str> = line.split("` VALUES ").collect();
                if splited.len() == 1 {
                    multiline_insert = true;
                    continue;
                }
                process_values(splited[1]);
            } else if line.starts_with("LOCK TABLES") {
                lock_write = true;
            } else if lock_write {
                if line == "UNLOCK TABLES" {
                    lock_write = false;
                    continue;
                }
                let mut chars = line.chars();
                chars.next_back();
                if chars.as_str().starts_with('(') {
                    process_values(&chars.as_str());
                }
            }
        }
    }

    Ok(())
}
